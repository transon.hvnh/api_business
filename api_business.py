from flask import Flask, request
import json
from service import *
from flask_cors import CORS
import socket
app = Flask(__name__)
CORS(app)


@app.route("/data_api", methods=["POST"])
def test_api():
    data = json.loads(request.data)
    data = get_info(data['business_code'].strip())
    return data

if __name__ == "__main__":
    app.run(host='116.118.48.172',port=9999, debug=True)

