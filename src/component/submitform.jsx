import "bootstrap/dist/css/bootstrap.min.css";
import Informap from "./infor";
import "./page.css";
import React, { useState} from "react";
import axios from "axios";

function CreateInfor(data) {
  return (
    <Informap
      key={data.id}
      name_company={data.name_company}
      name_company_english={data.name_company_english}
      name_shortcut={data.name_shortcut}
      status={data.status}
      business_code={data.business_number}
      legal_type={data.legal_type}
      created_date={data.start_date}
      ceo_name={data.name_founder}
      addr={data.add_ho}
    />
  );
}

// var data = {
//   name_company: "CÔNG TY TNHH ABC TECHNOLOGY",
//   name_company_english: "ABC TECHNOLOGY COMPANY LIMITED ",
//   name_shortcut: "ABC TECH ",
//   status: "Đang hoạt động",
//   business_number: "0314541137",
//   legal_type: "Công ty trách nhiệm hữu hạn hai thành viên trở lên",
//   start_date: "27/07/2017",
//   name_founder: "TRẦN THANH TÙNG",
//   add_ho:
//     "Tầng 2, 431A Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Thành phố Hồ Chí Minh, Việt Nam",
//   id: "1",
// };

function FromTest() {
  const [name, setName] = useState("");
  const [datFinal, setdataFinal] = useState([]);
  function handleChange(event) {
    setName(event.target.value);
  }

  // function HandleClick(event) {
  //   useEffect(async () => {
  //     const list = await axios({
  //       url: "http://192.168.1.14:9999/data_api",
  //       method: "POST",
  //       data: {
  //         business_code: name,
  //       },
  //     });
  //     setdataFinal(list.data);
  //   }, []);
  // }

  function HandleClick() {
    axios({
      method: 'post',
      url: 'http://116.118.48.172:9999/data_api',
      
      data: {
        business_code: name,
      }
    }).then((response) => {
      setdataFinal(response.data);
    })
  }

  return (
    <form className="area">
      <p>Nhập mã doanh nghiệp tại đây</p>
      <div class="col-lg-2 col-md-3">
        <input
          class="form-control"
          type="text"
          placeholder="Example: 0101580685"
          aria-label="default input example"
          value={name}
          onChange={handleChange}
        />
        <button type="button" class="btn btn-primary" onClick={HandleClick}>
          Submit
        </button>
      </div>
      {[datFinal].map(CreateInfor)}
    </form>
  );
}

export default FromTest;
