import React from "react";

function Informap(props) {
  return (
    <div class="col-lg-6 col-md-9">
      <ol class="list-group list-group-numbered">
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Tên đầy đủ của công ty</div>
            {props.name_company}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Tên công ty bằng tiếng anh</div>
            {props.name_company_english}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Tên viết tắt</div>
            {props.name_shortcut}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Tình trạng hoạt động</div>
            {props.status}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Mã số doanh nghiệp</div>
            {props.business_code}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Loại hình pháp lý</div>
            {props.legal_type}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Ngày thành lập</div>
            {props.created_date}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Tên người sáng lập</div>
            {props.ceo_name}
          </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            <div class="fw-bold">Địa chỉ</div>
            {props.addr}
          </div>
        </li>
      </ol>
      
    </div>
  );
}

export default Informap;
