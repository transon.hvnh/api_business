import time
from selenium import webdriver
from bs4 import BeautifulSoup

json_data = {
    "name_company": "",
    "name_company_english":"",
    "name_shortcut":"",
    "status":"",
    "business_number": "",
    "legal_type":"",
    "start_date":"",
    "name_founder":"",
    "add_ho":"",
    "id":"1"
}

def get_info(business_number):

    json_data['business_number'] = business_number
    html = 'https://dichvuthongtin.dkkd.gov.vn/inf/default.aspx'
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("disable-gpu")
    options.add_argument('window-size=1920x1080')
    options.add_argument('headless')

    browser = webdriver.Chrome('./chromedriver',chrome_options=options)
    browser.get(html)

    time.sleep(3)
    # search box business
    search_box = browser.find_element_by_xpath('//input[@name="ctl00$FldSearch"]')
    search_box.send_keys(business_number)
    submit = browser.find_element_by_xpath('//input[@name="ctl00$btnSearch"]').click()

    #click business name
    time.sleep(2)
    try:
        business_value = browser.find_elements_by_link_text(business_number)[0].click()
    except:
        return {"business_code": business_number, "error_type":"khong co ma doanh nghiep nay"}
        browser.close()

    #path infor
    time.sleep(3)
    source_html = browser.page_source
    soup = BeautifulSoup(source_html, 'html.parser')

    def get_text(id, data):
        name = soup.find('span', {'id': id}).get_text()
        json_data[data] = name

    #ten danh nghiep
    get_text('ctl00_C_NAMEFld', 'name_company')

    #ten doanh nghiep nuoc ngoai
    get_text('ctl00_C_NAME_FFld', 'name_company_english')

    #ten doanh nghiep viet tat
    get_text('ctl00_C_SHORT_NAMEFld', 'name_shortcut')

    #tinh trang hoat dong
    get_text('ctl00_C_STATUSNAMEFld', 'status')

    #tinh trang phap ly
    get_text('ctl00_C_ENTERPRISE_TYPEFld', 'legal_type')

    #ngay hoat dong
    get_text('ctl00_C_FOUNDING_DATE', 'start_date')

    #dia chi
    get_text('ctl00_C_HO_ADDRESS', 'add_ho')

    #name_founder
    name = soup.find_all('span',{'class': 'viewInput viewSearch'})[-1].get_text().strip()
    json_data['name_founder'] = name
    browser.close()
    return json_data
